//Since this project is flow-sensitive, this testcase contains a if-else statement. The program should know a could be either null or not null, which leads to a null pointer error.

class Condition {
    public static void main(String[] s){
		int x;
		A a;
		B b;

		x = 1;

		if (x < 0) {
			//b.m() is not null
			b = new B();
		} else {
			//b.m() is null
			b = new C();
		}

		a = b.m();
        System.out.println((a.n()));
    }
}

class A {
	public int n() {
		return 0;
	}
}

class B {
	A a;
	public A m() {
		a = new A();
		return a;
	}
}

class C extends B {
	public A m() {
		return a;
	}
}
