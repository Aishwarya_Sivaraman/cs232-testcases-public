/* This tests whether types flow back from message sends, including an odd
   edge case for fields from base classes. */
class Main {
    public static void main(String[] args) {
        A a;
        a = new A();
        System.out.println(a.f(a));
    }
}
class Base {
	X x;
	public int g() {
        x = x.getX();
        return 0;
    }
}
class A extends Base {
    public int f(A a) {
	    int blah;
        x = new X();
		blah = a.g();
        return blah + (x.m());
    }
}
class X {
    X x;
    public X getX() {
        return x;
    }
    public int m() {
        return 42;
    }
}