// This test case tests checking the return value after returning from a class method.

class NullReturnValue {

    public static void main (String [] args) {

        Sample aObj;
        Sample bObj;
        int ret;

        aObj = new Sample();
        ret = aObj.setValue(10);

        // bObj with null return value
        bObj = aObj.getSample();
        ret = bObj.printValue();
    }
}


class Sample {
    Sample nullObj;
    int value;

    public int setValue(int iValue) {
        value = iValue;

        return value;
    }

    public int printValue() {
        System.out.println(value);

        return value;
    }

    public Sample getSample() {
        return nullObj;
    }
}
