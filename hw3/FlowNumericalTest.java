/*
** Test inheritance and condition inside condition
** for numerical analysis, simple recursive
*/


class FlowNumericalTest {
  public static void main(String[] a){
    System.out.println(new A().printif(8));
    System.out.println(new A().printif(3));
  }
}

class A{
  B b;
  int tmp;

  public int printif(int num) {
    if (num < 6) {
      b = new B().initialValue();
      tmp = b.returnThree();
      if (tmp < 4) {
        tmp = b.returnTwo();
        tmp = tmp - 1;
        System.out.println(tmp);
      } else {
        System.out.println(b.returnTwo());
      }
    } else {
      System.out.println(new A().printif(num - 7)); 
    }
    return new C().returnThree();
  }

  public B initialValue() {
    B c;
    c = new B();
    return c;
  }

  public int returnThree() {
    return 1;
  }
}

class B extends A{
  public int returnTwo() {
    return 2;
  }
  public int returnThree() {
    return 3;
  }
}

class C extends B{
  public int returnTwo() {
    return 10;
  }
}