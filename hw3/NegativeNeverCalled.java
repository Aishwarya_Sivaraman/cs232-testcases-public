// In this example the negative print statement in 
// class A will never be called.
// Because numerical analysis is context sensitive,
// so it wouldn't check. And it should be marked safe.
// In class B, it should evaluate that it always prints
// positive numbers.

class NegativeNeverCalled
{
	public static void main(String[] args)
	{
		B b;
		b = new B();
		System.out.println(0);
	}
}

class A 
{
	int a;
	public int printNegative()
	{
		a = 1;
		if (a < 0)
		{
			System.out.println(a);	
		}
		else
		{
			a = a - 2;
			System.out.println(a);
		}
		return a;
	}
}

class B 
{
	int b;
	public int printPositive()
	{
		b = 5;
		if (b < 0)
		{
			System.out.println(b);
		}
		else
		{
			b = b + 2;
			System.out.println(b);
		}
		return b;
	}
}