/*
This test checks if the analysis looks at both branches of an if-statement
and takes the least-upper bound properly to show that printing a negative number
is possible.
*/

class Main {
    public static void main(String[] a){
        int x;
        int y;
        A alpha;
        alpha = new A();
        x = 4;
        if (alpha.getTrue()) {
            y = alpha.getPositive();
        } else {
            y = alpha.getNegative();
        }

        x = x - y;
        System.out.println(x);
    }
}

class A {
    public boolean getTrue() {
        return true;
    }

    public int getNegative() {
        return 0 - 5;
    }

    public int getPositive() {
        return 5;
    }
}