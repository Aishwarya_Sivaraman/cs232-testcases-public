//checks that the analysis can process control flow appropriately

class Main
{
    public static void main (String[] args)
    {
        int a;
        a = 2;

        if (0) {
            a = a - 3;
        } else {
            a = a + 2;
        }
        a = a + 2;
        System.out.println(a);

        while (0) {
            a = a * (1 - 2);
        }
        a = a + 7;
        System.out.println(a);
    }
}